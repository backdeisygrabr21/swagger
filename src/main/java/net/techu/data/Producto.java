package net.techu.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@ApiModel
public class Producto {
    public String id;
    public String nombre;
    public Double precio;
    public String categoria;

    public Producto(String id,
                    String nombre,
                    double precio,
                    String categoria) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
    }

}
