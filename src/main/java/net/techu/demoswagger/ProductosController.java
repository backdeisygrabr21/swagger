package net.techu.demoswagger;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.techu.data.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductosController {

    private ArrayList<Producto> listaProductos;
    public ProductosController(){
        listaProductos = new ArrayList<Producto>();
        listaProductos.add(new Producto("PR1", "Producto PR1", 123.21, "CAT 1"));
        listaProductos.add(new Producto("PR2", "Producto PR1", 123.21, "CAT 1"));
        listaProductos.add(new Producto("PR3", "Producto PR1", 123.21, "CAT 1"));
    }

    @GetMapping("/productos")
    public ResponseEntity<ArrayList<Producto>> getListarProductos(){
        return new ResponseEntity<ArrayList<Producto>>(listaProductos, HttpStatus.OK);
    }

    @PostMapping("/productos/byname")
    @ApiOperation(value="Crear producto por nombre", notes="Este método crea un producto solo con el nombre")
    public ResponseEntity<Producto> addProductoByName(@ApiParam(name="nombre",
            type="String",
            value="nombre del producto a crear",
            example="PR1",
            required = true) @RequestParam String nombre)
    {
        Producto productoNuevo = new Producto("123", nombre, 122, "CATX");
        listaProductos.add(productoNuevo);
        return new ResponseEntity<Producto>(productoNuevo, HttpStatus.CREATED);
    }
}
